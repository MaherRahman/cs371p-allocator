// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <iostream> // cout, endl, getline
#include <stdexcept> // invalid_argument

using namespace std;

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }
public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

    // -----
    // read_head
    // -----

    /**
     * goes to the header and reads the value placed there
     */
    int read_head(pointer p) {
        // Move the pointer p back by 4 to read the Header value
        return *(ptr_add<int>(p, -4));
    }

    // -----
    // set_head
    // -----

    /**
     * sets a value of the header with a given size
     */
    void set_head(pointer p, int size) {
        int* header = ptr_add<int>(p, -4);
        *header = size;

        // Set the header of the next block (The Footer)
        int abs_size = size;
        if (abs_size < 0) {
            abs_size = -abs_size;
        }
        int* footer =  ptr_add<int>(p, abs_size);
        *footer = size;
    }


private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // ptr_add
    // -----

    /**
     * Adds an offset to pointer p and returns a pointer of a user set type PT
     */
    template<typename PT>
    PT* ptr_add(pointer p, int offset) {
        return (PT*)(((char*) p) + offset);
    }

    // -----
    // valid
    // -----
    /**
     * O(1) in space
     * O(n) in time
     * Ensure there are no illegal blocks.
     */
    bool valid ()  {
        int size = 0;
        iterator b(begin(a));
        iterator e(end(a));
        while(b != e) {
            // block size plus the size of the Header and Footer
            int block_size = read_head(*b);
            if (block_size < 0) {
                block_size = -block_size;
            }
            size += block_size + 8;
            ++b;
        }
        assert(b == e);
        // If size isn't equal to N by the end, something went wrong.
        return size == N;
    }

public:
    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return lhs._p == rhs._p;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        char* _p;

        // -----
        // ptr_add
        // -----

        template<typename PT>
        PT* ptr_add(pointer p, int offset) {
            return (PT*)(((char*) p) + offset);
        }

    public:
        // -----------
        // constructor
        // -----------

        iterator (const char* p) {
            _p = (char*) p;
        }

        iterator (pointer block_p) {
            _p = ptr_add<char>(block_p, -4);
        }

        // ----------
        // operator *
        // ----------

        pointer operator * ()  {
            return (pointer)(_p + 4);
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            int block_size = *((int*) _p);
            if (block_size < 0) {
                block_size = -block_size;
            }
            // Move to the next block by adding block_size, plus Header and Footer
            _p += block_size + 8;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }
    };


    // ------------
    // constructors
    // ------------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        // Initially create a block of the entire allocator size.
        set_head((pointer) (a + 4), N - 8);
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    pointer allocate_block(int size_to_allocate, iterator b) {
        // We found a block to allocate!!!!
        size_t block_size = read_head(*b);
        size_t excess = block_size - size_to_allocate;
        if(excess < sizeof(T) + 8) {
            // Not enough leftover space, give the whole block.
            set_head(*b, -block_size);
        } else {
            // The excess space is sufficient to make another block.
            set_head(*b, -size_to_allocate);
            ++b;
            // Create new block with the excess size.
            set_head(*b, excess - 8);
        }
        assert(*b != nullptr);
        return *b;
    }

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type s) {
        assert(s != 0);
        // Size is the number of units multiplied by the size of
        // each unit, denoted by sizeof(T)
        int size_to_allocate = s * sizeof(T);
        iterator b(begin(a));
        iterator e(end(a));
        pointer block_to_allocate = nullptr;
        // Keep looking until we find a block, or we reach the end.
        while(b != e && block_to_allocate == nullptr) {
            if(size_to_allocate <= read_head(*b)) {
                block_to_allocate = allocate_block(size_to_allocate, b);
            } else {
                // This block is no good, look at the next block.
                ++b;
            }
        }
        assert(valid());
        return block_to_allocate;
    }

    void print_(ostream& output) {
        iterator b(begin(a));
        iterator e(end(a));
        while(b != e) {
            output << read_head(*b) << " ";
            ++b;
        }
        assert(b == e);
    }

    // ---------
    // construct

    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());                               // from the prohibition of new
    }

    // ----------
    // check_left
    // ----------

    /**
     * Check the block to the left, and merge if it is free
     */
    pointer get_left_pointer(pointer p) {
        // when there is a block on the left
        char* prev_location = ptr_add<char>(p, -((int) sizeof(T) + 12));
        // If the location to the left exists, and isn't before a
        // meaning it isn't out of bounds.
        int prev_size = 0;
        if(begin(a) <= prev_location) {
            prev_size = *(ptr_add<int>(p, -8));
        }
        // If prev_size is positive, meaning block is free to use.
        if(prev_size > 0) {
            return (pointer) (ptr_add<char>(p, -(prev_size + 8)));
        }
        else {
            return p;
        }
    }

    // ------
    // merge
    // ------

    /**
     * Logic for merging the left and right blocks
     */
    void merge(pointer prev, pointer p) {
        // keep looking rightwards, and merging if free.
        iterator e(end(a));
        iterator next(p);
        // If initially next and prev are pointing to the same place, move next up.
        if (*next == prev) {
            ++next;
        }
        while(next != e && read_head(*next) > 0) {
            // Size is the total size from the left block, plus right block, plus the header and footer.
            size_t prev_size = read_head(prev);
            size_t next_size = read_head(*next);
            size_t new_size = prev_size + next_size + 8;
            set_head(prev, new_size);
            ++next;
        }
    }

    // --------------------
    // merge_left_and_right
    // --------------------

    /**
     * Looks to the left, and right, and merges the blocks if possible
     */
    void merge_left_and_right(pointer p) {
        pointer prev = get_left_pointer(p);
        merge(prev, p);
    }

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * Read Header and Footer, and if positive, then combine.
     * checking the left block and right block for combining.
     */
    void deallocate (pointer p, size_type s) {
        // The block should be allocated. Cannot deallocate something
        // That is not already allocated.
        // assert(read_head(p) < 0);
        int head_val = read_head(p);
        // Flip the head value to mark it as deallocated.
        set_head(p, -head_val);

        merge_left_and_right(p);
        assert(valid());
    }

    // ----------
    // deallocate
    // ----------

    /**
     * deallocate calls deallocate at a specified position
     */
    void deallocate(int pos) {
        iterator b(begin(a));
        iterator e(end(a));
        int counter = 0;
        // Keep finding blocks until we reach the block at the given position specified.
        while(b != e && counter < pos) {
            if(read_head(*b) < 0) {
                ++counter;
                if(counter == pos) {
                    // We found the block we want to deallocate!
                    deallocate(*b, -read_head(*b));
                }
            }
            ++b;
        }
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }
};

#endif // Allocator_h