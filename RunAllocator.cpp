// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Allocator.hpp"

// ----
// main
// ----

int main () {
    using namespace std;
    istream& input = cin;
    ostream& output = cout;

    string line;
    getline(input, line);
    int num_tests = stoi(line);
    input.ignore();
    for(int test = 0; test < num_tests; ++test) {
        my_allocator<double, 1000> allocator;
        // Keep reading until EOF or blank line.
        getline(input, line);
        while(!line.empty() && !input.fail()) {
            int allocate = stoi(line);
            if (allocate > 0) {
                // Allocate if the input is positive, otherwise, deallocate.
                allocator.allocate(allocate);
            } else {
                // Flip the sign, then send it to deallocate.
                allocate = -allocate;
                allocator.deallocate(allocate);
            }
            getline(input, line);
        }
        allocator.print_(output);
        output << endl;
    }
    return 0;
}