# CS371p: Object-Oriented Programming Allocator Repo

* Name: Maher Rahman

* EID: mmr3447

* GitLab ID: @MaherRahman

* HackerRank ID: MaherRahman1

* Git SHA: fb7c14050d1ec8d58f0b8c68069a0270e6c32138

* GitLab Pipelines: https://gitlab.com/MaherRahman/cs371p-allocator/pipelines

* Estimated completion time: 22 hours

* Actual completion time: 30

* Comments: Because of Corona causing weird complications with groupwork. Instead of working
* in partners like I had intended, I ended up being alone and having to 
* work on this assignment by myself. It was much harder and without guidance
* and the assistance from peers and having to use a much worse laptop before
* I could set up SSH, I found this assignment much harder than the previous ones. 
